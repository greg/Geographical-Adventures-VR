using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleHud : MonoBehaviour
{
    public bool isActive;
    public List<GameObject> hud;

    void Start()
    {
        isActive = true;
        GameObject[] hudArray = GameObject.FindGameObjectsWithTag("Hiddable");
        hud = new List<GameObject>(hudArray.Length);

        foreach (GameObject obj in hudArray)
        {
            hud.Add(obj);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            isActive = !isActive;

            foreach (GameObject element in hud)
            {
                element.SetActive(isActive);
            }
        }
    }
}
