using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR;
using UnityEngine.XR.OpenXR;
using System.Linq;

public class PlayerInputHandler : MonoBehaviour
{

	public Player player;
	public GeoGame.Quest.QuestSystem questSystem;
	public GameCamera gameCamera;
	public UIManager uIManager;
	public SolarSystem.SolarSystemManager solarSystemManager;
	PlayerAction playerActions;

// VR
	List<UnityEngine.XR.InputDevice> leftHandDevices;
	List<UnityEngine.XR.InputDevice> rightHandDevices;
	Vector2 movementInput = Vector2.zero;
	Vector2 joystick;
	bool drop_package;
	public bool boost;
	public bool boosting = false;
// END VR

	void Start()
	{
		playerActions = RebindManager.Instance.activePlayerActions;

		playerActions.PlayerControls.Enable();
		playerActions.CameraControls.Enable();
		playerActions.UIControls.Enable();

		rightHandDevices = new List<UnityEngine.XR.InputDevice>();
		leftHandDevices = new List<UnityEngine.XR.InputDevice>();
	}


	void Update()
	{
		if (GameController.IsState(GameState.Playing))
		{
			PlayerControls();
			CameraControls();
			SolarSystemControls();
		}

		UIControls();
	}

	void PlayerControls()
	{
		// BASE
		// Vector2 movementInput = playerActions.PlayerControls.Movement.ReadValue<Vector2>();
		
// VR
		// MOVEMENT + BOOST
		UnityEngine.XR.InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.LeftHand, leftHandDevices);
		if (leftHandDevices.Count > 0)
		{
			UnityEngine.XR.InputDevice device = leftHandDevices[0];
			if (device.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxis, out joystick))
			{
				movementInput = joystick;
			}
			if (device.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out boost))
			{
				boosting = boost;
			}
		}
		// DROP PACKAGE
		UnityEngine.XR.InputDevices.GetDevicesAtXRNode(UnityEngine.XR.XRNode.RightHand, rightHandDevices);
		if (rightHandDevices.Count > 0)
		{
			UnityEngine.XR.InputDevice device = rightHandDevices[0];
			if (device.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out drop_package))
			{
				if (drop_package)
				{
					questSystem.TryDropPackage();
				}
			}
		}
// FIN VR

		float accelerateDir = playerActions.PlayerControls.Speed.ReadValue<float>();
		// BASE
		// bool boosting = playerActions.PlayerControls.Boost.IsPressed();
		player.UpdateMovementInput(movementInput, accelerateDir, boosting);

		if (playerActions.PlayerControls.DropPackage.WasPressedThisFrame())
		{
			questSystem.TryDropPackage();
		}
	}

	void SolarSystemControls()
	{
		if (playerActions.PlayerControls.MakeDaytime.WasPressedThisFrame())
		{
			solarSystemManager.FastForward(toDaytime: true);
		}
		if (playerActions.PlayerControls.MakeNighttime.WasPressedThisFrame())
		{
			solarSystemManager.FastForward(toDaytime: false);
		}
	}

	void CameraControls()
	{
		if (playerActions.CameraControls.ForwardCameraView.WasPressedThisFrame())
		{
			gameCamera.SetActiveView(GameCamera.ViewMode.LookingForward);
		}
		if (playerActions.CameraControls.BackwardCameraView.WasPressedThisFrame())
		{
			gameCamera.SetActiveView(GameCamera.ViewMode.LookingBehind);
		}
		if (playerActions.CameraControls.TopCameraView.WasPressedThisFrame())
		{
			gameCamera.SetActiveView(GameCamera.ViewMode.TopDown);
		}
	}

	void UIControls()
	{
		if (playerActions.UIControls.TogglePause.WasPressedThisFrame())
		{
			uIManager.TogglePause();
		}

		if (playerActions.UIControls.ToggleMap.WasPressedThisFrame())
		{
			uIManager.ToggleMap();
		}
	}

}
