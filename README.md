# Geographical Adventures
A little work-in-progress geography game about delivering packages to different countries around the world. </br>
Made in the Unity game engine.

You can try playing the game in its current state [here.](https://sebastian.itch.io/geographical-adventures)
![Image](https://raw.githubusercontent.com/SebLague/Images/master/Geographical%20Adventures.jpg)
![Image](https://raw.githubusercontent.com/SebLague/Images/master/Geographical%20Adventures%202.jpg)

# Development Videos
[Episode 01: I Tried Creating a Game Using Real-World Geographic Data](https://youtu.be/sLqXFF8mlEU) </br>
[Episode 02: Trying to Improve My Geography Game with More Real-World Data](https://youtu.be/UXD97l7ZT0w) </br>
[Episode 03: Polishing and Releasing My Little Geography Game](https://www.youtube.com/watch?v=pNp4ug5F6To)

# VR version
This is a fork that implement VR with OpenXR. 
- No motion controllers
- No speedometer or compass
- Add the TAB key to toggle on/off the timer and quest objectives for immersion.

![Image](https://www.gregandev.fr/images/projets/geo-vr.png)

## TO DO 

- [X] HUD resize (QuestUI higher, timer lower)
- [X] Support for XR controllers
- [X] Remove turbo timed limit
- [ ] Handle menus

## bugs

- [X] Mesage UI broken
- [ ] Messages ghosting (need timer)